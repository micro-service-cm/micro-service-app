package com.haydikodlayalim.accountservice.accountservice.service;

import com.haydikodlayalim.accountservice.accountservice.entity.Account;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.support.SpringFactoriesLoader;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;


@RequiredArgsConstructor //Gereken constuctor varsa inject et.
@Service //Servis özellikle belli iş mantığı barındırdan metodlarımızı barındıran yer olmalı.
//@Component //Web bileşenlerimizi içieriside Utility gibi kullanabileceğimiz classlara vereceğimiz anatasyon
//@Repository //Entegreasyon için örneğin veritabanına çıkış yapılacağında kullanılacak bir anatasyon
public class AccountService {


    private  final AccountRepository accountRepository;

    public Account get(String id){

        //return accountRepository.findById(id).orElseThrow(() -> new IllegalArgumentException());
        return new Account("test-id");
    }
    public Account save(Account account){

        return accountRepository.save(account);

    }
    public Account update(String id, Account account){
        Assert.isNull(id , "Id cannot be null");
        get(id);
        return accountRepository.save(account);

    }
    public void delete(String id){

    }

    public List<Account> findAll(){
        return accountRepository.findAll();
    }



}
