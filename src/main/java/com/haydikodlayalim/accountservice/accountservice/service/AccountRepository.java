package com.haydikodlayalim.accountservice.accountservice.service;

import com.haydikodlayalim.accountservice.accountservice.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface AccountRepository extends JpaRepository<Account, String> {


}
