package com.haydikodlayalim.accountservice.accountservice.entity;

import jakarta.persistence.Column;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of ="id")
@ToString
@Entity
@Table(name = "accounts")
public class Account implements Serializable {

  /*
  @Id
  @GeneratedValue(generator = "uuid")
  @GenericGenerator(name = "uuid", strategy = "uuid")
  @Column(name = "uuid", length = 32)

   */

  public Account(String id) {
    this.id = id;
  }

  @Id
  @GeneratedValue(generator = "uuid")
  @GenericGenerator(name = "uuid", strategy = "uuid")
  @Column(name = "id")
  private String id = UUID.randomUUID().toString();

  @Setter
  @Column(name = "uname")
  private String username;

  @Setter
  @Column(name = "email")
  private String email;

  @Setter
  @Column(name = "pwd")
  private String password;

  @Column(name = "created_at")
  private Date cretedAt;

  @Column(name = "is_active")
  private  Boolean active;
}
