package com.haydikodlayalim.accountservice.accountservice.api;

import com.haydikodlayalim.accountservice.accountservice.entity.Account;
import com.haydikodlayalim.accountservice.accountservice.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.awt.print.Pageable;
import java.util.List;

@RestController //Dış dünyaya açar web api olduğunu belirtir.
@RequestMapping ("account")// Url vermemize sağlar. Adres için gerekli

/**
 * localhost:8080/account
 * HTTP METODLARI : GET PUT  DELETE  POST
 */

public class AccountApi {


    @Autowired //İnject et
    AccountService accountService; //Artık bu servis içinde bir context var bunu bana ver diyoruz.

    //Spring Best practies
    //constructor injection
    public AccountApi(AccountService accountService){
        this.accountService = accountService;
    }

    @GetMapping("/{id}") //path veriyoruz.
    public ResponseEntity<Account>get(@PathVariable("id") String id)
    //Tek parametre verirken @PathVariable veriyoruz.
    // Birden fazla parametre vereceksek @PathVariable("id") ile GetMapping içerisinde verdiğimiz parametre ismini {id} veriyoruz.
    {
       return ResponseEntity.ok(accountService.get(id));
    }

    @PostMapping()
    public ResponseEntity<Account>save(@RequestBody Account account) {
        return ResponseEntity.ok(accountService.save(account));
    }

    @PutMapping()
    public ResponseEntity<Account>update(@PathVariable("id") String id,  @RequestBody Account account) {
        return ResponseEntity.ok(accountService.update(id, account));
    }

    @DeleteMapping
    public void delete(String id) {

        accountService.delete(id);
    }

    @GetMapping("/getAll")
    public ResponseEntity<List<Account>> getAll(){
        return ResponseEntity.ok(accountService.findAll());
    }

}
